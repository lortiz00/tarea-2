<?php 
 
require_once 'db_conexion.php';
 
if($_GET['id']) {
    $id = $_GET['id'];
 
    $sql = "SELECT * FROM libreria WHERE id = {$id}";
    $result = $connect->query($sql);
 
    $data = $result->fetch_assoc();
 
    $connect->close();
 
?>
 
<!DOCTYPE html>
<html>
<head>
    <title>Editar libro</title>
 
    <style type="text/css">
        fieldset {
            margin: auto;
            margin-top: 100px;
            width: 50%;
        }
 
        table tr th {
            padding-top: 20px;
        }
    </style>
 
</head>
<body>
 
<fieldset>
    <legend>Editar libro</legend>
 
    <form action="actioneditar.php" method="post">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <th>Titulo</th>
                <td><input type="text" name="titulo"  value="<?php echo $data['titulo'] ?>" /></td>
            </tr>     
            <tr>
                <th>Autor</th>
                <td><input type="text" name="autor"  value="<?php echo $data['autor'] ?>" /></td>
            </tr>
            <tr>
                <th>Editorial</th>
                <td><input type="text" name="editorial"  value="<?php echo $data['editorial'] ?>" /></td>
            </tr>
            <tr>
                <th>Precio</th>
                <td><input type="text" name="precio"  value="<?php echo $data['precio'] ?>" /></td>
            </tr>
            <tr>
                <input type="hidden" name="id" value="<?php echo $data['id']?>" />
                <td><button type="submit">Salvar cambios</button></td>
                <td><a href="index.php"><button type="button">volver</button></a></td>
            </tr>
        </table>
    </form>
 
</fieldset>
 
</body>
</html>
 
<?php
}
?>