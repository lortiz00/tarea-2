<!DOCTYPE html>
<html>
<head>
    <title>Agregar Libro</title>
 
    <style type="text/css">
        fieldset {
            margin: auto;
            margin-top: 100px;
            width: 50%;
        }
 
        table tr th {
            padding-top: 20px;
        }
    </style>
 
</head>
<body>
 
<fieldset>
    <legend>Agregar libro</legend>
 
    <form action="createlibro.php" method="post">
        <table cellspacing="0" cellpadding="0">
            <tr>
                <th>titulo</th>
                <td><input type="text" name="titulo"  /></td>
            </tr>     
            <tr>
                <th>autor</th>
                <td><input type="text" name="autor"  /></td>
            </tr>
            <tr>
                <th>editorial</th>
                <td><input type="text" name="editorial"  /></td>
            </tr>
            <tr>
                <th>precio</th>
                <td><input type="text" name="precio"  /></td>
            </tr>
            <tr>
                <td><button type="submit">salvar cambios</button></td>
                <td><a href="index.php"><button type="button">volver</button></a></td>
            </tr>
        </table>
    </form>
 
</fieldset>
 
</body>
</html>