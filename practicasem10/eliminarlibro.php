<?php 
 
require_once 'db_conexion.php';
 
if($_GET['id']) {
    $id = $_GET['id'];
 
    $sql = "SELECT * FROM libreria WHERE id = {$id}";
    $result = $connect->query($sql);
    $data = $result->fetch_assoc();
 
    $connect->close();
?>
 
<!DOCTYPE html>
<html>
<head>
    <title>Eliminar libro</title>
</head>
<body>
 
<h3>Realmente lo desea eliminar?</h3>
<form action="actioneliminar.php" method="post">
 
    <input type="hidden" name="id" value="<?php echo $data['id'] ?>" />
    <button type="submit">Si, eliminar</button>
    <a href="index.php"><button type="button">No, volver</button></a>
</form>
 
</body>
</html>
 
<?php
}
?>