-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 27-11-2018 a las 18:08:40
-- Versión del servidor: 10.1.36-MariaDB
-- Versión de PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbbiblioteca`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_aumentar` (IN `aporcentaje` INT(10), IN `aeditorial` VARCHAR(250))  begin
  update libreria set precio=precio+(precio*aporcentaje/100)
  where editorial=aeditorial;
 end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_multiplicar10` (IN `aeditorial` VARCHAR(250))  begin
 
  update libreria set precio=precio+(precio*0.10)
  where editorial=aeditorial;
 end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libreria`
--

CREATE TABLE `libreria` (
  `id` int(11) NOT NULL,
  `titulo` varchar(40) NOT NULL,
  `autor` varchar(30) NOT NULL,
  `editorial` varchar(20) NOT NULL,
  `precio` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `libreria`
--

INSERT INTO `libreria` (`id`, `titulo`, `autor`, `editorial`, `precio`) VALUES
(7, 'Uno', 'Richard Bach', 'Planeta', '19.97'),
(8, 'Ilusiones', 'Richard Bach', 'Planeta', '15.97'),
(9, 'El aleph', 'Borges', 'Emece', '27.50'),
(10, 'Aprenda PHP', 'Mario Molina', 'Nuevo siglo', '50.00'),
(11, 'Matematica estas ahi', 'Paenza', 'Nuevo siglo', '18.00'),
(12, 'Puente al infinito', 'Bach Richard', 'Sudamericana', '14.00'),
(13, 'Antología', 'J. L. Borges', 'Paidos', '24.00'),
(14, 'Java en 10 minutos', 'Mario Molina', 'Siglo XXI', '45.00'),
(15, 'Cervantes y el quijote', 'Borges- Casares', 'Planeta', '45.25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `id` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`id`, `email`, `password`) VALUES
(1, 'admin@admin.com', 'd033e22ae348aeb5660fc2140aec35850c4da997'),
(2, 'administrator@admin.com', '014436b6640304b2cfad8a43f4aaad1a');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `libreria`
--
ALTER TABLE `libreria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `libreria`
--
ALTER TABLE `libreria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `login`
--
ALTER TABLE `login`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
