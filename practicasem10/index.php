<?php require_once 'db_conexion.php'; ?>
 
<!DOCTYPE html>
<html>
<head>
    <title>Libros</title>
 
    <style type="text/css">
        .manageMember {
            width: 50%;
            margin: auto;
        }
 
        table {
            width: 100%;
            margin-top: 20px;
        }
 
    </style>
 
</head>
<body>
 
<div class="manageMember">
    <a href="create.php"><button type="button">Agregar libro</button></a>
    <table border="1" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th>Titulo</th>
                <th>Autor</th>
                <th>Editorial</th>
                <th>Precio</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $sql = "SELECT * FROM libreria";
            $result = $connect->query($sql);
 
            if($result->num_rows> 0) {
                while($row = $result->fetch_assoc()) {
                    echo "<tr>
                        <td>".$row['titulo']."</td>
						<td>".$row['autor']."</td>
                        <td>".$row['editorial']."</td>
                        <td>".$row['precio']."</td>
                        
                          <td>  <a href='editarlibro.php?id=".$row['id']."'><button type='button'>Editar</button></a></td>
                          <td> <a href='eliminarlibro.php?id=".$row['id']."'><button type='button'>Eliminar</button></a></td>
                        </td>
                    </tr>";
                }
            } else {
                echo "<tr><td colspan='5'><center>No Data Avaliable</center></td></tr>";
            }
            ?> 
			 
			 
			 
			 
        </tbody>
    </table>
</div>
 
</body>
</html>